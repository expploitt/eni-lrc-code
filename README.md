# Locally Recoverable Code implementation
Implementation of a LRC code with parameters [n, k, r] = [6, 4, 2] for python v. 3.7.

##Coder
To encode a file, use the following command in a terminal
```
python3 coder.py -f <path to file to encode>
```      
The created files will be save in the "docs/" directory.

##Decoder
To recover and decode a file use the following command
```
python3 decoder.py -f <path and name of the file with its extension>
```      
For example
```
python3 decoder.py -f files/image_recovered.jpeg
```      

