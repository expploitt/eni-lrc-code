import numpy as np
import json
import argparse
from pyfinite import ffield
from datetime import datetime

r = 2
k = 4
n = 6


def interpolador(x, recov_values):
    val = 0
    F = ffield.FField(4)
    for j in range(0, r):
        val = F.Add(val, F.Multiply(recov_values[j][1], lagrange(x, j, recov_values)))
    return val


def lagrange(x, j, recov_values):
    val = 1
    F = ffield.FField(4)
    for k in range(0, r):
        if k != j:
            val = F.Multiply(val, F.Divide(F.Subtract(x, recov_values[k][0]),
                                           F.Subtract(recov_values[j][0], recov_values[k][0])))
    return val


def read_sharps(number_of_sharps):
    rows = []
    erased_sharps = []
    erased_sharp = -1
    for i in range(number_of_sharps):
        with open("docs/TestFile.shard" + str(i), "r") as f:
            a = f.readline().rstrip().split('#')
            a.pop()
            if not a:
                erased_sharps.append(i)
            else:
                row = np.array(a, dtype=int).astype(np.int)
                rows.append(row)

    if erased_sharp:
        for i in erased_sharps:
            rows.insert(i, np.zeros(len(rows[0])))
    return rows, erased_sharps


def get_matrix(rows, n, m):
    elements = np.array([], dtype=int)
    for i in range(n):
        elements = np.append(elements, rows[i])
    matrix = np.reshape(elements, (n, m))
    return matrix


def bitstring_to_bytes(s):
    return int(s, 2).to_bytes((len(s) // 8), byteorder='big')


def main():
    parser = argparse.ArgumentParser(description='LRC (n=6, k=4, r=2) decoder')
    parser.add_argument('-f', required=True, help='name/path to file where you want to recover the information')

    args = parser.parse_args()

    print()
    print('--------------------------')
    print('Decoder LRC(6, 4, 2)')
    print('--------------------------')
    print()

    group_values = np.array([4, 11, 15, 2, 12, 14], dtype=int)
    init_recover = datetime.now()
    recovered_sharps, erased_sharp_indexes = read_sharps(6)

    if len(erased_sharp_indexes) > 2:
        print('Imposible to recover, it has been losted more than 2 files.')
        print()
        exit()

    s = len(recovered_sharps[0])
    matrix = get_matrix(recovered_sharps, 6, s)

    for erased_sharp_index in erased_sharp_indexes:
        print()
        print('The shard ', erased_sharp_index, ' has been removed...')
        print('Recovering file shard', erased_sharp_index)

        percent = 0

        for i in range(0, s):
            if erased_sharp_index == -1:
                break

            recovered_values = np.zeros([6, 2])
            recovered_values[:, 1] = np.array(
                [matrix[0][i], matrix[1][i], matrix[2][i], matrix[3][i], matrix[4][i], matrix[5][i]], dtype=int)
            recovered_values[:, 0] = group_values
            recovered_values = recovered_values.astype(int)

            if erased_sharp_index == 0:
                recovered_values[erased_sharp_index][1] = interpolador(recovered_values[erased_sharp_index][0],
                                                                       recovered_values[1:3])
            elif erased_sharp_index == 1:
                recovered_values[erased_sharp_index][1] = interpolador(recovered_values[erased_sharp_index][0],
                                                                       np.array([recovered_values[0], recovered_values[2]],
                                                                                dtype=int))
            elif erased_sharp_index == 2:
                recovered_values[erased_sharp_index][1] = interpolador(recovered_values[erased_sharp_index][0],
                                                                       recovered_values[0:2])
            elif erased_sharp_index == 3:
                recovered_values[erased_sharp_index][1] = interpolador(recovered_values[erased_sharp_index][0],
                                                                       recovered_values[4:6])
            elif erased_sharp_index == 4:
                recovered_values[erased_sharp_index][1] = interpolador(recovered_values[erased_sharp_index][0],
                                                                       np.array([recovered_values[3], recovered_values[5]],
                                                                                dtype=int))
            elif erased_sharp_index == 5:
                recovered_values[erased_sharp_index][1] = interpolador(recovered_values[erased_sharp_index][0],
                                                                       recovered_values[3:5])

            matrix[:, i] = recovered_values[:, 1]

            percent += 1
            print('{0:.2f}'.format(percent / s * 100), ' % recovered...', end='\r')

        print('{0:.2f}'.format(percent / s * 100), ' % recovered...')

    final_recover = datetime.now()

    with open('docs/RecoverFile.json', 'r') as f:
        json_file = json.loads(f.read())

    f = open(args.f, "w+b")
    f.close()

    f = open(args.f, "ab")

    bits = np.array([], dtype=int)

    print()
    print('Translating codewords to original words...')
    init_trans = datetime.now()
    for i in np.transpose(matrix):
        string = json_file[str(i.astype(int))].replace('[', '').replace(']', '')
        bits = np.append(bits, np.fromstring(string, dtype=int, sep=' '))
    final_trans = datetime.now()
    bits = bits.reshape((s, k))
    bits = np.transpose(bits)
    bits = bits.flatten()

    print('Saving into file...')
    percent = 0
    for i in np.arange(0, len(bits), 1000):
        bits_string = ''.join(map(str, np.array2string(bits[i:(i + 1000)]))).replace('[', '').replace(']', '').replace(
            ' ', '').replace('\n', '')
        f.write(bitstring_to_bytes(bits_string))
        percent += 1
    f.close()

    print('Decoding finished')
    print()

    print('Time spent in recovering: ', final_recover - init_recover)
    print('Time spent in decoding: ', final_trans - init_trans)


if __name__ == "__main__":
    main()
