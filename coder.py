from fileinput import filename

import numpy as np
import json
import argparse
from pyfinite import ffield
import os
from datetime import datetime

r = 2


def pow(b, n):
    F = ffield.FField(4)
    if n == 1:
        value = 1
    else:
        value = b
        for i in range(n - 1):
            value = F.Multiply(value, b)

    return value


def fa(x, msg):
    F = ffield.FField(4)
    val = 0
    for i in range(r - 1 + 1):
        val = F.Add(val, F.Multiply(f_i(x, i, msg), pow(x, i)))
    return val


def f_i(x, i, msg):
    F = ffield.FField(4)
    val = 0
    for j in range(int(r - 1) + 1):
        val = F.Add(val, F.Multiply(msg[i][j], pow(g(x), j)))
    return val


def g(x):
    return pow(x, 3)


def get_sharps(k, file_name):
    print('Dividing file, getting 6 shards...')
    b = np.fromfile(file_name, dtype="uint8")
    bits = np.unpackbits(b)

    if len(bits) % k:
        a = k - (len(bits) % k)
        np.append(bits, np.zeros(a))

    s = int(len(bits) / k)
    elements = np.reshape(bits, (k, s))   # matrix [k, s]

    return elements, s


def encode(b1, b2, b3, b4, k, s, json_file, i):
    # Reformateamos  el mensaje a matriz

    l1 = np.array([b1, b2])
    l2 = np.array([b3, b4])
    msg = np.array([l1, l2], dtype=int)

    x = np.array([4, 11, 15, 2, 12, 14], dtype=int)
    result = np.zeros(len(x))
    for i in range(len(x)):
        result[i] = fa(x[i], msg)

    json_file[''.join(map(str, np.array2string(result.astype(int))))] = ''.join(
        map(str, np.array2string(np.array([b1, b2, b3, b4], dtype=int))))
    write_to_files(result.astype(int))


def write_to_files(result):
    for i in range(len(result)):
        f = open('docs/TestFile.shard' + str(i), 'a')
        f.write(str(result[i]) + '#')
        f.close()


def create_sharp_files(n):
    print('Creating sharp files...')

    os.makedirs('docs', exist_ok=True)

    for i in range(n):
        f = open('docs/TestFile.shard' + str(i), 'w+')
        f.close()


def main():
    parser = argparse.ArgumentParser(description='LRC (n=6, k=4, r=2) coder')
    parser.add_argument('-f', required=True, help='Path to file')

    args = parser.parse_args()

    print('------------------------')
    print('Coder LRC(6, 4, 2)')
    print('------------------------')
    print()
    print('Inicializando...')

    k = 4
    n = 6

    init_recover = datetime.now()
    create_sharp_files(6)
    bits, s = get_sharps(k, args.f)

    with open("docs/RecoverFile.json", 'w+', encoding='utf-8') as f1:
        json_file = dict()
        percent = 0

        print('Encoding words...')

        for i in range(0, s):
            encode(bits[0][i], bits[1][i], bits[2][i], bits[3][i], k, s, json_file, i)
            percent += 1
            print('{0:.2f}'.format(percent / s * 100), ' % encoded...', end='\r')

        final_recover = datetime.now()

        print('{0:3.2f}'.format(percent / s * 100), ' % encoded...')
        print('Saving to files...')

        json.dump(json_file, f1, ensure_ascii=False, indent=4)

    print('Coding finished!')
    print()
    print('Time spent in encoding: ', final_recover - init_recover)


if __name__ == "__main__":
    main()
